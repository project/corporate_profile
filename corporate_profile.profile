<?php

/**
 * @file
 * Enables modules and site configuration for corporate site installation.
 */

/**
 * Select the current install profile by default.
 */
if (!function_exists("system_form_alter")) {

  /**
   * Implements hook_form_alter().
   */
  function system_form_install_select_profile_form_alter(&$form, $form_state) {
    foreach ($form['profile'] as $profile_name => $profile_data) {
      $form['profile'][$profile_name]['#value'] = 'corporate_profile';
    }
  }

}

/**
 * Implements hook_install_tasks().
 */
function corporate_profile_install_tasks($install_state) {
  $tasks = array();
  $tasks['corporate_profile_blocks_turning'] = array(
    'display' => FALSE,
  );
  $tasks['corporate_profile_public_files_copy'] = array(
    'display' => FALSE,
  );
  $tasks['corporate_profile_config'] = array(
    'display_name' => st('Corporate Profile Config'),
    'display' => TRUE,
    'type' => 'form',
    'run' => INSTALL_TASK_RUN_IF_NOT_COMPLETED,
    'function' => 'corporate_profile_config_form',
  );
  return $tasks;
}

/**
 * Our custom form step.
 *
 * Allow user to select profile features and demo content.
 *
 * @param array $form_state
 *   Form state.
 *
 * @return array
 *   Form.
 */
function corporate_profile_config_form(array $form_state) {
  $form['content'] = array(
    '#type' => 'fieldset',
    '#title' => st('Test content'),
  );
  $form['content']['components'] = array(
    '#type' => 'checkboxes',
    '#options' => array(
      'yes' => st('Would you like to install test content?'),
    ),
    '#default_value' => array(
      'yes',
    ),
    '#description' => st('Help to provide all functionality of this profile. Uncheked if you are skilled user and don\'t need it.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => st('Apply'),
  );
  return $form;
}

/**
 * Corporate Profile configuration form submit.
 *
 * Enable features modules depending on users' choice.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   Form state.
 */
function corporate_profile_config_form_submit(array $form, array &$form_state) {
  $modules = array();
  $values = $form_state['values'];
  if ($values['components']['yes']) {
    $modules[] = 'content';
  }
  // Enable features.
  module_enable($modules);
}

/**
 * Our custom task.
 *
 * Enable and turning blocks.
 *
 * @param array $install_state
 *   An array of information about the current
 *   installation state.
 *
 * @throws \Exception
 */
function corporate_profile_blocks_turning(array $install_state) {
  $node = node_load(1);
  if (!empty($node)) {
    $node->path = array(
      'alias' => 'contact',
      'pathauto' => FALSE,
      'language' => 'und',
    );
    node_save($node);
  }
  if (!module_exists('blocks')) {
    $modules = array('blocks');
    module_enable($modules);
  }
  $view = views_get_view('front_carousel');
  $view->style_options['instance'] = 'owlcarousel_settings_default';
  $view->save();
}

/**
 * Our custom task.
 *
 * Copy public files for default theme.
 *
 * @param array $install_state
 *   An array of information about the current
 *   installation state.
 */
function corporate_profile_public_files_copy(array $install_state) {
  $source = 'profiles/corporate_profile/uuid_features_assets/adaptivetheme/';
  $res = 'sites/default/files/adaptivetheme/';
  corporate_profile_recurse_copy($source, $res);
  $image_source = 'profiles/corporate_profile/uuid_features_assets/theme_logo.png';
  $image_res = 'sites/default/files/theme_logo.png';
  copy($image_source, $image_res);
  $image_source = 'profiles/corporate_profile/uuid_features_assets/avatar_default.png';
  $image_res = 'public://avatar_default.png';
  copy($image_source, $image_res);
}

/**
 * Recursive copy.
 *
 * @param string $src
 *   - Source folder with files.
 * @param string $dst
 *   - Destination folder.
 */
function corporate_profile_recurse_copy($src, $dst) {
  $dir = opendir($src);
  @mkdir($dst);
  while (FALSE !== ($file = readdir($dir))) {
    if (($file != '.') && ($file != '..')) {
      if (is_dir($src . '/' . $file)) {
        corporate_profile_recurse_copy($src . '/' . $file, $dst . '/' . $file);
      }
      else {
        copy($src . '/' . $file, $dst . '/' . $file);
      }
    }
  }
  closedir($dir);
}
