Corporate profile
==================

Corporate profile is a simple and clean multi-purpose profile using Clean Corporate theme by ADCI Solutions.

It can be used for professional business and corporate sites, portfolios, and promotion of services. The theme is fully responsive and looks nice from any device.

We’ve packed the profile with a variety of blocks, several page layouts including full-width page, one and two sidebars page, a nice portfolio page and a blog including the comments option.

Corporate profile is very easy to install and use, you can try to install it yourself following the instructions below.

Notes:
Installation of test content may take some time. If you get the “Maximum execution time...” error, need to increase the max_execution_time parameter in php.ini file.
There is the “Twitter Feed” block that’s included in the corporate profile (take a look at the website footer). At this moment it’s just a dummy block. You can use it as needed. We want to add a full-featured “Twitter Feed” block in the future.

To install the profile:

- Unzip the folder and copy it to necessary folder of your server.
- Install your new website with the corporate profile. It’s set by default, you just need to click the “Save and continue” button and follow the instructions.
- Wait for the corporate profile to be installed, enable the necessary modules and enjoy your website.

You can install the corporate profile without test content. To do this, simply uncheck the “Would you like to install test content?” checkbox during the installation process.

Thank you for choosing Corporate profile!

Created by ADCI solutions team
Supporting organizations: 
ADCI Solutions

